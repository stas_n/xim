class Numeric
  def look_and_say
    Enumerator.new do |y|
      el = self
      loop do
        y << el
        el = el.to_s.chars
                 .chunk { |digit| digit }
                 .map { |digit, list| list.length.to_s + digit }
                 .join.to_i
      end
    end
  end
end
