require_relative '../patched_numeric'

describe Numeric do
  it 'should return sequence' do
	expect(1.look_and_say.each.take(5)).to match_array([1, 11, 21, 1211, 111221])
	expect(10.look_and_say.each.take(3)).to match_array([10, 1110, 3110])
  end
end
