Allow to use look and say sequence for integers

`1.look_and_say.each.take(5)`

 => [1, 11, 21, 1211, 111221]